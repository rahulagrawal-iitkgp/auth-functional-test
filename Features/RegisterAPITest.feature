Feature: Register API test

  Scenario Outline: New user registering with correct credentials
    Given register api endpoint
    And param user_name as <user_name>
    And param email_id as <user_email>
    And param password as <password>
    When user hits post method with params
    Then verify that response http status code is <status_code>

     Examples:
       | user_name                  | user_email         | password           | status_code |
       | test1                      | t1@gmail.com       | test@1             | 200         |


  Scenario Outline: New user registering with incorrect email format
    Given register api endpoint
    And param user_name as <user_name>
    And param email_id as <user_email>
    And param password as <password>
    When user hits post method with params
    Then verify that response http status code is <status_code>
    And  verify that response content detail is <status_detail>
    And verify that response content detail is <status_detail>

     Examples:
       | user_name                  | user_email         | password           | status_code | status_detail |
       | test2                      | tgmail.com         | test@1             | 400         |  The email address is not valid. It must have exactly one @-sign.             |
       | test3                      | t@gmailcom         | test@1             | 400         |  The part after the @-sign is not valid. It should have a period.             |


  Scenario Outline: New user registering with not unique username
    Given register api endpoint
    And param user_name as <user_name>
    And param email_id as <user_email>
    And param password as <password>
    When user hits post method with params
    Then verify that response http status code is <status_code>
     And verify that response content detail is <status_detail>

     Examples:
       | user_name                  | user_email         | password           | status_code | status_detail |
       | test1                      | t@gmail.com         | test@1             | 400         | Username is already taken |



Feature: Test API test
  Scenario Outline: Existing user login without registration and then immediately use the access token to hit test api
    Given login api endpoint
    And param username as <user_name>
    And param password as <password>
    And user hits login with form params
    And parse the access token from the response
    When hit the get method test api with access token
    Then verify that response http status code is <status_code>

     Examples:
       | user_name                   | password           | status_code |
       | test1                       | test@1             | 200         |

  Scenario Outline: Existing user login without registration and then after timeout use the access token to hit test api
    Given login api endpoint
    And param username as <user_name>
    And param password as <password>
    And user hits login with form params
    And parse the access token from the response
    And wait for <delay> sec
    When hit the get method test api with access token
    Then verify that response http status code is <status_code>
    And verify that response content detail is <status_detail>

     Examples:
       | user_name                   | password           | status_code | delay | status_detail|
       | test1                       | test@1             | 401         |   11  |  Re-login access token not valid|
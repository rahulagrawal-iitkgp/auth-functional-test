from behave import given, when, then
import requests
import json
import time

api_base_url = "http://127.0.0.1:8000/"
params = {}

@given(u'{endpoint} api endpoint')
def step_impl(context, endpoint):
    context.url = api_base_url + endpoint + '/'

@given(u'param {param_key} as {param_value}')
def step_impl(context, param_key, param_value):
    params[param_key] = param_value
    context.data = params

@when(u'user hits post method with params')
def step_impl(context):
    context.response = requests.post(context.url, json=context.data)

@when(u'user hits post method with form params')
def step_impl(context):
    context.response = requests.post(context.url, data=context.data)

@then(u'verify that response http status code is {response_status}')
def step_impl(context, response_status):
    assert int(context.response.status_code) == int(response_status)

@then(u'verify that response content detail is {status_detail}')
def step_impl(context, status_detail):
    assert json.loads(context.response.content)['detail'] == status_detail

@given(u'user hits login with form params')
def step_impl(context):
    context.login_response = requests.post(context.url, data=context.data)

@given(u'parse the access token from the response')
def step_impl(context):
    context.access_token = json.loads(context.login_response.content)['access_token']

@when(u'hit the get method {endpoint} api with access token')
def step_impl(context, endpoint):
    url = api_base_url + endpoint + "/"
    token = context.access_token
    headers = {'Authorization': "Bearer {}".format(token)}
    context.response = requests.get(url, headers=headers)

@given(u'wait for {delay} sec')
def step_impl(context, delay):
    time.sleep(int(delay))






Feature: Login API test
  Scenario Outline: New user login without registration
    Given login api endpoint
    And param username as <user_name>
    And param password as <password>
    When user hits post method with form params
    Then verify that response http status code is <status_code>
    And verify that response content detail is <status_detail>

     Examples:
       | user_name                   | password           | status_code | status_detail                  |
       | test2                       | test@1             | 401         | Incorrect username or password |

  Scenario Outline: user login with wrong password
    Given login api endpoint
    And param username as <user_name>
    And param password as <password>
    When user hits post method with form params
    Then verify that response http status code is <status_code>
    And verify that response content detail is <status_detail>

     Examples:
       | user_name                   | password           | status_code | status_detail                  |
       | test1                       | test@2             | 401         | Incorrect username or password |

  Scenario Outline: user login with correct credentials
    Given login api endpoint
    And param username as <user_name>
    And param password as <password>
    When user hits post method with form params
    Then verify that response http status code is <status_code>

     Examples:
       | user_name                   | password           | status_code |
       | test1                       | test@1             | 200         |